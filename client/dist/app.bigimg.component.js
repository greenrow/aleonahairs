"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AppBigimgComponent = /** @class */ (function () {
    function AppBigimgComponent() {
        this.yy = 6;
        this.canshow_chChange = new core_1.EventEmitter();
        this.descfilesChange = new core_1.EventEmitter();
    }
    AppBigimgComponent.prototype.close_proj = function (e) {
        this.descfiles = '';
        this.canshow_ch = false;
        this.canshow_chChange.emit(false);
        this.descfilesChange.emit('');
    };
    AppBigimgComponent.prototype.moveslider = function (e) {
        var target = e.target.classList;
        var elem_width = 100 * this.fileslength;
        var current_pos = parseInt($('.project_foto_no_fixed_width').attr('l'));
        if (target.contains('arrow_right')) {
            if (current_pos == -(elem_width - 200)) {
                $('.arrow_right').addClass('hide');
            }
            current_pos = current_pos - 100;
            $('.arrow_left').removeClass('hide');
            $('.project_foto_no_fixed_width').css('left', current_pos + '%');
            $('.project_foto_no_fixed_width').attr('l', current_pos);
        }
        if (target.contains('arrow_left')) {
            if (current_pos == -100) {
                $('.arrow_left').addClass('hide');
            }
            if ($('.arrow_right').hasClass('hide')) {
                $('.arrow_right').removeClass('hide');
            }
            current_pos += 100;
            $('.project_foto_no_fixed_width').css('left', current_pos + '%');
            $('.project_foto_no_fixed_width').attr('l', current_pos);
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], AppBigimgComponent.prototype, "yy", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], AppBigimgComponent.prototype, "canshow_ch", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], AppBigimgComponent.prototype, "descfiles", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], AppBigimgComponent.prototype, "descfiles1", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], AppBigimgComponent.prototype, "fileslength", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], AppBigimgComponent.prototype, "canshow_chChange", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], AppBigimgComponent.prototype, "descfilesChange", void 0);
    AppBigimgComponent = __decorate([
        core_1.Component({
            selector: 'project_desc',
            template: "\n   <div class='project_foto_wrap'[style.display]=\"canshow_ch ? 'block' : 'none'\"  >\n <div *ngIf='canshow_ch' (click)='moveslider($event)' class='glyphicon glyphicon-chevron-right arrow arrow_right'></div><div *ngIf='canshow_ch' (click)='moveslider($event)' class='glyphicon glyphicon-chevron-left arrow arrow_left hide' ></div>\n    <p (click)=\"close_proj($event)\" class='close_parent'>x</p>\n\n      <div  *ngIf='canshow_ch' [attr.l]='0' class='project_foto_no_fixed_width' [style.left.px]='0'  [style.width.%]='100*fileslength' >    \n   <div  class='project_item' [style.width.%]=\"(100/fileslength)\" *ngFor=\"let image of descfiles\">\n\n           <div class='image_back'[ngStyle]= \"{'background': 'url('+image.path+') no-repeat center/contain'}\">1</div>\n           <div class='image_desctiption'><p>{{image.desc}}</p></div>\n                   </div>\n                   </div>\n                     </div>\n                     \n      "
        })
    ], AppBigimgComponent);
    return AppBigimgComponent;
}());
exports.AppBigimgComponent = AppBigimgComponent;
