"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var core_2 = require("@angular/core");
var slider = require("./anim_slider");
var http_2 = require("@angular/http");
var Hero = /** @class */ (function () {
    function Hero() {
    }
    return Hero;
}());
exports.Hero = Hero;
var Imgpath = /** @class */ (function () {
    function Imgpath() {
    }
    return Imgpath;
}());
exports.Imgpath = Imgpath;
var ARR = [];
var AppComponent = /** @class */ (function () {
    function AppComponent(_http) {
        var _this = this;
        this._http = _http;
        this.img_desc = 'poor';
        this.xxxx = { id: 1 };
        this.anim = slider.animate;
        this.iss = true;
        this.title = 'tesr of Heroes';
        this.test = 'Template <script>alert("evil never sleeps")</script>Syntax';
        this.canshow = false;
        this._this = this;
        this.newshow = false;
        this.i = 2;
        _http.get('/files/').subscribe(function (res) {
            console.log(res);
            var item = JSON.parse(res["_body"]);
            console.info('ok loaded!');
            _this.projectarr = item[0];
            _this.newsarr = item[1];
            //this.iss=true;
            setTimeout(function () { _this.iss = false; }, 2000);
        }, //For Success Response
        function (//For Success Response
        err) { console.error(err); }); //For Error Response);
    }
    //events// 
    AppComponent.prototype.clickev = function (ev) {
        var _this = this;
        var target_ = ev.target;
        while (!target_.classList.contains('project_images')) {
            target_ = target_.parentNode;
        }
        if (target_.classList.contains('project_images')) {
            this.canshow = !this.canshow;
            var regexp = /\/static\/public\/projects\/(.+)\/(.+)/g;
            var id = target_.id;
            var qp = new http_2.URLSearchParams();
            qp.append('proj_id', id);
            var options = new http_2.RequestOptions({ search: qp });
            this._http.get('/files/proj/', options).subscribe(function (res) {
                var item = JSON.parse(res['_body']);
                var files_arr = item[0].files;
                _this.fileslength = files_arr.length;
                _this.descfiles = files_arr;
            });
        }
    };
    AppComponent.prototype.getnews = function (e) {
        var _this = this;
        var news_id = e.target.nodeName == "ARTICLE" ? e.target.id : e.target.parentNode.id;
        var qp = new http_2.URLSearchParams();
        qp.append('news_id', news_id);
        var options = new http_2.RequestOptions({ search: qp });
        this._http.get('/files/getnews/', options).subscribe(function (res) {
            _this.newshow = true;
            var item = JSON.parse(res['_body']);
            _this.newscontent = item[0].content;
            window.scrollTo(0, 0);
        });
    };
    AppComponent.prototype.closenews = function (e) {
        var item = e.target.getAttribute('data-click-item');
        this[item] = false;
    };
    AppComponent.prototype.ngAfterViewInit = function () {
        var evt = new Event("angularloaded", { "bubbles": true, "cancelable": false });
        setTimeout(function () {
            document.dispatchEvent(evt);
        }, 50);
    };
    ;
    AppComponent = __decorate([
        core_1.Component({
            selector: "appy",
            templateUrl: "/static/views/layout.html"
        }),
        core_2.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
