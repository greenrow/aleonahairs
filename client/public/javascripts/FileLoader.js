 // namespace FileLoader
 var FileLoader=function (){
      var thisarg=arguments;
        this.img_length;
        this.upload_arr=[];
        this.files;
        this.area_root;
        var _loopmode;
       //errors
        if(thisarg[2] !=undefined && typeof(thisarg[1]) !='object'){console.error('ошибка во втором параметhе .неверно софрмирован обькт');return false;}

        //check args
        if(thisarg[0] != undefined){
            console.info('type of 1 arg ',typeof(thisarg[0]));
            this.root=thisarg[0].trim();
    
        }else{console.error('FileLoader error : не найден родительский контейнер');return false;}
        console.info('root',this.root)
        this.img_length=document.querySelectorAll(this.root+' img').length
        this.files=document.querySelectorAll(this.root+' img');
        this.loadevent() //run load event listener

    }
    
  /**********call area obj***********/  
FileLoader.prototype.area=function(root){
    if(root !=undefined){
        console.info('Area creating');
          return new FileLoader.Area(this,root)
    }else{
           console.error('не уазано обязательное свойство root');
           return false;
    }
}
          
  /**********area instn obj***********/  
FileLoader.Area_instance=function(_this,root){
    if(root =='undefined'){console.error('не указан родитель area')}
 var el=document.querySelector(root);
  var buttons=document.createElement('div');
  buttons.classList.add('jsfl-buttons-wrap');
  buttons.setAttribute('style','position:absolute;bottom:-60px;height:60px;width:100%;padding:0px;background-color:#2C2C30;z-index:100')
  var files_wrap=document.createElement('div');
  files_wrap.setAttribute('style','width:100%;overflow:hidden;margin-bottom:70px;height:inherit');
  var file=document.createElement('div');
  file.classList.add('jsfl-files-wrap');
  file.setAttribute('style','overflow:auto;width:calc(100% + 20px);max-height:400px;padding:15px;height:auto;');
  files_wrap.appendChild(file)
  
   el.appendChild(buttons);
   _this.areaactive=1;
   el.appendChild(files_wrap);
   
    this.area_root=root;
    this.files_root=file;
    this.buttons_root=buttons;
  el.style.display='block';
   //event listeners 
}

  /**********static Area instn obj***********/  
FileLoader.Area_instance.getInstance=function(_this,root){
    document.querySelector(root).style.display='block';


    if(!this.static){
        
        this.static=new this(_this,root);
    }
    return this.static
}

  /**********triiger event load listener***********/  
FileLoader.prototype.loadevent=function(){
           var _this=this;
           this.no_loaded_img=[];
                var length=_this.img_length;
                for(var i=length-1; i>=0;i--){
                    if(_this.hidefoto){_this.files[i].style.opacity='0'}
                    if( _this.files[i].complete== false){
                        this.no_loaded_img.push({'not loaded':i})
                        _this.files[i].setAttribute('fljs-img-index',i)
                        _this.files[i].addEventListener('load',function(){
                                _this.upload_arr.push('image loaded');
                       _this.files[i].setAttribute('fljs-img-loaded')
                        })
                    }
                 }
                 
                 console.info('this.no_loaded_img length',this.no_loaded_img.length)
 }
 
         /**********success***********/  
 FileLoader.prototype.success=function(){
            var _this=this;
            var args=arguments
            setTimeout(function(){
                console.info('length',_this.no_loaded_img.length,'arr l',_this.upload_arr.length)
                if(_this.upload_arr.length ==  _this.no_loaded_img.length ){//if all images loaded
                        setTimeout(function(){
                            if(args[0] !=undefined){
                               
                            console.info('ALL IMAGES UPLOADED')
                            console.info('size of loadd_arr',_this.upload_arr.length)
                            console.info('size of files length',_this.no_loaded_img.length );
                             args[0] ();
                            }
                           
                        },400);
  
                }else{
                     setTimeout(arguments.callee,10)
                }
            },10)
           
  }
         
 /**********area obj***********/  
        
FileLoader.Area=function(_this,root){
    var files_length=_this.no_loaded_img.length;
    var file_upload_arr=_this.upload_arr;
    var area_root;
    var area_obj=0;
    var btn_obj=0;
    var _this=_this;
    var file_obj_root=_this.root;
    this.setStatus=function(item,val){
                switch(item){
                    case 'area':area_obj=val;break;
                    case 'btn':btn_obj=val;break;
                }
    }
            this.setItem=function(item,val){
                
                switch(item){
                    case 'area_root':
                      area_root=val ;break; 
                }
                
            }
            
            this.getItem=function(item){
                
                switch(item){
                    case 'area_root':
                      return root;break;
                    case 'upload_arr':
                      return file_upload_arr;break;
                    case 'files_length':
                      return files_length;break;
                    case 'main_root':
                      return file_obj_root;break;
                      
                }
                
            }
            this.getStatus=function(item){
                switch(item){

                    case 'area':
                        return area_obj ;break;
                    case 'btn':
                        return btn_obj;break;
                    case 'parent':
                        return _this;


                    case 'root':
                        return root;
        
                }
            }


   //return static obj
  this.area_ins=FileLoader.Area_instance.getInstance(this,root)
 }
          /**********call btn obj***********/  
    FileLoader.Area.prototype.buttons=function(arg){
           FileLoader.Buttons.getInstance(this,arg)
   }
   FileLoader.Area.prototype.files=function(arg){
       
       if(typeof(arg) !='object'){console.error('files должно быть обьектом');return false;}
       
       return new FileLoader.Files(this,arg)
   }
   

  /**********buton obj***********/  
FileLoader.Buttons=function(_this,arg){
        var btn_area=_this.area_ins.buttons_root;
        var area=_this.area_ins.area_root;
             this.it=0;
                 this.st=0;
                  if( !this.it){

                    var btn_arr=arg.split(',');
                    var change_recurse=0;
                    for(var z=0;z<btn_arr.length;z++){
                            console.info('btn val',btn_arr[z] )
                               if(btn_arr[z] == 'close') {
                                    var text=document.createTextNode('Закрыть');
                                    var close_div=document.createElement('div');
                                    close_div.classList.add('jsfl-btn','jsfl-close-file')
                                    close_div.appendChild(text);
                                    close_div.addEventListener('click',function(){
                                            var main_root=document.querySelector(_this.getItem('main_root'))
                                            var main_root_l=main_root.children.length;
                                             var f_length=document.querySelector('.jsfl-files-wrap').children.length;
                                             console.info('jsf l',f_length,'all l',main_root_l)
                                            if(main_root_l==f_length ){ ;main_root.innerHTML=''}else{
                                                for(var g=main_root_l; g > main_root_l-f_length; g--){
                                                    main_root.removeChild(main_root.children[g-1])
                                           }
                                            
                                        }
                                         document.querySelector('.jsfl-files-wrap').innerHTML='';document.querySelector(area).style.display='none'

                                     })
                                       btn_area.appendChild(close_div);   
                               }
                          
                                if(btn_arr[z] == 'load'){
                                    var load_text=document.createTextNode('Загрузить');
                                    var load_div=document.createElement('div');
                                  
                                    load_div.appendChild(load_text);
                                    load_div.classList.add('jsfl-btn','fileloader-load-btn')
                                     btn_area.appendChild(load_div);
                                  }
                                if(btn_arr[z] == 'save'){
                        
                                    var save_text=document.createTextNode('Сохранить')
                                    var save_div=document.createElement('div');
                                    save_div.classList.add('jsfl-btn','jsfl-save-file');
                                    save_div.addEventListener('click',function(){
                      

                                        //add textarea
                                    var text_area=document.querySelector('.jsfl-files-wrap:first-child textarea');
                                 
                                    var area_root=document.querySelector('.jsfl-files-wrap')
                               
                                    var main_root=document.querySelector(_this.getItem('main_root'))
                             
                                    var main_root_img=main_root.getElementsByTagName('img');
                                    var main_root_l=main_root_img.length;
                                    var f_length=document.querySelector('.jsfl-files-wrap').children.length;
                                    var textarea_all=area_root.querySelectorAll('textarea');
                                 
                                     var j =f_length-1
                                    if(text_area !=null){
                                           if(f_length == main_root_l){ 
                                               for(var i=0; i < main_root_l; i++){
                                                    var textarea_val=textarea_all[i].value;
                                                    var textarea;
                                                    textarea_all[i].getAttribute('rewrite') ? textarea=document.createElement('textarea') : textarea=document.createElement('p');
                                                     textarea.setAttribute('style','width:100%')
                                                     textarea.classList.add('jsfl-desc')
                                                    var txt=document.createTextNode(textarea_val);
                                                    textarea.appendChild(txt);
                                                    main_root_img[i].parentNode.insertBefore(textarea,main_root_img[i].nextSibling);
                                                  
                                                };
                                                 console.info('root h '+main_root.offsetHeight+ ' wind h '+window.innerHeight)
//                                                            if(main_root.offsetHeight >window.innerHeight){
//                                                                    var timerID = setInterval(function() {
//                                                                        window.scrollBy(0, 80);
//                                                                        if( window.pageYOffset >= main_root.offsetHeight-100 )
//                                                                            clearInterval(timerID);
//                                                                    }, 13);
//                                                            }
                                            }else{
                                                   var diff=main_root_l-f_length;
                                                    for(var i=main_root_l; i >diff; i--){
                                                        var textarea_val=textarea_all[j].value
                                                         var textarea;
                                                    textarea_all[j].getAttribute('rewrite') ? textarea=document.createElement('textarea') : textarea=document.createElement('p');
                                                    textarea.setAttribute('style','width:100%');
                                                        textarea.classList.add('jsfl-desc')
                                                        var txt=document.createTextNode(textarea_val);
                                                        textarea.appendChild(txt);
                                                        main_root_img[i-1].parentNode.insertBefore(textarea,main_root_img[i-1].nextSibling);
                                                        j--;
                                                     }
                                                     console.info('root h '+main_root.offsetHeight+ ' wind h '+window.innerHeight)
                                                            if(main_root.offsetHeight >window.innerHeight){
                                                                    var timerID = setInterval(function() {
                                                                        window.scrollBy(0, 80);
                                                                        if( window.pageYOffset >= main_root.offsetHeight-100 )
                                                                            clearInterval(timerID);
                                                                    }, 13);
                                                            }
                                                            
                        
                                            }
                                    }
                                                  document.querySelector('.jsfl-files-wrap').innerHTML='';document.querySelector(area).style.display='none';

                                    }
                                            
                        
                        )
                                  
                                    save_div.appendChild(save_text);
                                     btn_area.appendChild(save_div);
                                }

                    }
                    
                    if(btn_arr.length>0){
                        //all btn style in one
                        var btncss=document.createElement('style');
                        var btnclass=document.createTextNode(".jsfl-btn-hover{background-color:#331D1D !important}");
                        btncss.appendChild(btnclass)
                        var btnarr=document.querySelectorAll('.jsfl-btn');
                        document.querySelector(area).appendChild( btncss);
                        Array.prototype.forEach.call(btnarr,function(element){
                        var btnwrap_h=document.querySelector('.jsfl-buttons-wrap').style.height;

                            element.setAttribute('style','line-height:'+btnwrap_h+';cursor:pointer;background-color:#000;color:#fff;margin:0px;width:100px;height:100%;text-align:center;border-right:1px solid grey;border-radus:3px;bottom:0px;position:relative;float:right');
                            element.addEventListener('mouseover',function(){element.classList.add('jsfl-btn-hover')})
                            element.addEventListener('mouseleave',function(){element.classList.remove('jsfl-btn-hover')})
                        })
                    }
                    
            }
                
                    
                  
              
                this.it=1
            }
            
            /**********init static Button obj***********/  
FileLoader.Buttons.getInstance = function (_this,arg) {
            if (!this.instance) {
                this.instance = new this(_this,arg);
            }
            return this.instance;
};

   /**********butons***********/  
FileLoader.Area.prototype.buttons=function(arg){
             
                return FileLoader.Buttons.getInstance(this,arg)
            }
      
 

FileLoader.Files=function(_this,arg){
    if(arg.length ==0 || typeof(arg) !='object'){console.error('jsfl error неверно указан парметр обькта File');return false;}

    var file_upload_arr=_this.getItem('upload_arr');
    var files_length=_this.getItem('files_length');
   
    var main_area=_this.area_ins.files_root;
  var main_root=_this.getItem('main_root');
  var desc_rule;

    this.getItem=function(item){
        
        switch(item){
            case 'file-length':
                return files_length;break;
        
            case 'upload-arr':
                return file_upload_arr;break;
            case 'area-root':
                return main_area;break;
            case 'file-wrap':
                return main_wrap;break;
            case 'left-div':
                return leftdiv;break;
            case 'right-div':
                return rightdiv;break;
            case 'main-root':
                return main_root;break;
            case 'main-root-l':
                return document.querySelector(main_root).chidren.length
            case 'file-wrap':
                return main_wrap;

        }
    }
    
    this.setItem=function(item,val){
        switch(item){
            
         
        }
        
    }
 if(this.settings){
     
 
var x_l=document.querySelectorAll(main_root+' img').length
                  console.info('load')
                 var w;
         
       w= window.outerWidth>1500 ?'20%':'30%';        
                  
   for(var i=0;i<files_length;i++)  {   

                var main_wrap=document.createElement('div');
                main_wrap.classList.add('progress_files_wrap');
                main_wrap.setAttribute('style','width:100%;margin:15px 0px;position:relative;background-color:#201F30;box-shadow:         0px 1px 5px 0px rgba(7, 37, 44, 0.95);');
                if(arg['description'] != undefined){ main_wrap.style.height='100px'}
                else if(arg['foto'] != undefined ){main_wrap.style.height='80px'}else{main_wrap.style.height='60px'}
                var leftdiv=document.createElement('div');
                leftdiv.setAttribute('style','float:left;width:55%;height:100%;position:relative;')
                var rightdiv=document.createElement('div');
                rightdiv.setAttribute('style','float:right;width:'+w+';height:100%;')
                main_wrap.appendChild(leftdiv);
                main_wrap.appendChild(rightdiv);
                main_area.appendChild(main_wrap);
                for(var z in arg){
                    switch(z){
                        case 'name':
                            this.name(arg,i);break;
                        case 'progress':
                             this.progress(arg,i);break;
                        case 'foto':
                             this.foto(arg,i);break;
                        case 'description':
                             this.description(arg,i,x_l-files_length+i);break;
                        case 'delete':
                             this.delete(x_l-files_length+i);break;
                    }
                }
    }
}

}

FileLoader.Files.prototype.settings=function(arg)   {
    
    if(typeof(arg) !='object')  {console.log('lsfl-error','Files-Settings','неверный тип параметра,должен быть обьект');return false;}
    if(arg['desc-rewrite']==true){
        var txtarea_list=document.querySelectorAll('.jsfl_textarea');
        if(txtarea_list.length >0){
            var l=txtarea_list.length;
            for(var i=0;i<l;i++){
                txtarea_list[i].setAttribute('rewrite',1)
                
            }
        }
        }
                
 }
FileLoader.Files.prototype.delete=function(l){


    var del_text=document.createTextNode('x');
    var _this=this;
    var del_text_p=document.createElement('p');
    del_text_p.setAttribute('jsfl-pos',l)
    del_text_p.appendChild(del_text);
    del_text_p.classList.add('jsfl-del-item')
    del_text_p.setAttribute('style','font-weight:bold;color:#fff;font-size:1.2em;text-align:center;cursor:pointer;position:absolute;top:0px;z-index:100;width:100%;opacity:0.9');

       var wrapdiv=this.getItem('file-wrap');
       var wrap_div_h=wrapdiv.style.height;
    var opacity_div=document.createElement('div');
    opacity_div.setAttribute('style','opacity:0.7;background-color:#F90707;height:100%;width:100%;position:relative;top:0px;left:0px;');
        del_text_p.style.lineHeight=wrap_div_h;
       var del_div=document.createElement('div');
       del_div.setAttribute('style','position:absolute;right:0px;;top:0px;width:30px;height:100%;');
       del_div.appendChild(del_text_p)
       del_div.appendChild(opacity_div);
       wrapdiv.appendChild(del_div);
       
        del_text_p.addEventListener('click',function(){
            var elem=this;
            var elemindex_to_del=elem.getAttribute('jsfl-pos');
            wrapdiv.parentNode.removeChild(wrapdiv);
            var main_root_= document.querySelector(_this.getItem('main-root'))
            var item_to_remove=main_root_.querySelector("img[fljs-img-index='"+elemindex_to_del+"']");
            var parent=item_to_remove.parentNode;
            while(parent.parentNode !=  null){parent.parentNode.removeChild(parent); }
            parent.removeChild(item_to_remove)
    })
    
}
FileLoader.Files.prototype.name=function(arg,i){

     var rightdiv=this.getItem('right-div');

                var main_area=document.querySelector(this.getItem('main-area'));
                console.info('*******fileinfo active******');

                            var div=document.createElement('div');
                            div.classList.add('jsfl-filename');
                            div.setAttribute('style','text-overflow:ellipsis;width:inherit;;color:#fff;overflow:hidden;padding:0px;float:right;position:absolute;height:100%;');
                            var opacity_div=document.createElement('div');
                            opacity_div.setAttribute('style','width:100%;top:0px;left:0px;height:100%;background-color:#4E3A54;opacity:0.6');
                            div.appendChild(opacity_div);
                            var p_text=document.createElement('p')
                            var text=document.createTextNode(arg['name'][i]);
                            p_text.setAttribute('style','position:absolute;text-align:left;;top:0px;fon-weight:bold;z-index:90');
                            p_text.appendChild(text)
                            div.appendChild(p_text);
                             if(arg['foto'] ==undefined){
        
                                    div.style.height='100%'
                               }else{       div.style.height='30%'}
                              rightdiv.appendChild(div)
                              console.info('filnamename div created')
    
}     
FileLoader.Files.prototype.foto=function(arg,i){
    
             
                var main_area=document.querySelector(this.getItem('main-area'));
                     var rightdiv=this.getItem('right-div');
                console.info('*******fileinfo active******');

                              var div=document.createElement('div');
                              div.classList.add('fileloader_file_foto');
                              div.setAttribute('style','text-overflow:ellipsis;width:100%;background-color:#7F827F;color:#fff;overflow:hidden;padding:0px;float:right')
                              var img=document.createElement('img');
                              img.setAttribute('style','height:100%;width:100%')
                              img.setAttribute('src',arg['foto'][i])
                                 div.style.height='100%';
                              div.appendChild(img);
                              rightdiv.appendChild(div)
                              console.info('filnamename div created')
    
}   
FileLoader.Files.prototype.description=function(arg,i,y){
                    var main_area=document.querySelector(this.getItem('main-area'));
                    var leftdiv=this.getItem('left-div');
                console.info('*******fileinfo active******');
                              var div=document.createElement('textarea');
                               div.setAttribute('jsfl-pos',y)
                              div.classList.add('jsfl_textarea');
                              div.setAttribute('style','outline:none;border:none;background-color:#fff;width:100%;background-color:#fff;color:#000;overflow:hidden;padding:0px;height:60px;position:absolutelbottom:50px;')
                              div.setAttribute('placeholder','Введите описание файла');
                              leftdiv.appendChild(div)
                              console.info('filnamename div created')
}   


     /**********progress***********/  
FileLoader.Files.prototype.progress=function(arg,i){
    
     var leftdiv=this.getItem('left-div');      
                    console.info('*******progress active******');
    var file_upload_arr=this.getItem('upload-arr');
                   //progress
                var progress=document.createElement('div');
                progress.classList.add('file_progress');
                progress.setAttribute('style',"position:relative;left:0px;top:0px;height:inherit;width:0;background-color:#55B253");
                var progress_wrap=document.createElement('div');
                progress_wrap.classList.add('progress_wrap');
                progress_wrap.appendChild(progress);
                progress_wrap.setAttribute('style','border:1px solid grey;width:100%;position:relative;height:20px;margin:5px 0px 10px;top:0px;')
                leftdiv.appendChild(progress_wrap)

    //animation
    
    var _this=this;
               
   var anim=function (i){
            console.info('i',i)
            var start=new Date().getTime();
            var delay=50000;
            var result=0;
            var progress_el_list=document.querySelector('.jsfl-files-wrap').lastChild;
            console.info('progress parent',progress_el_list)
            var item=progress_el_list.getElementsByClassName('progress_wrap')[0].getElementsByClassName('file_progress')[0];
            console.info('progress ',item)
            var item_width=item.parentElement.offsetWidth;
            console.info('progress width',item_width)
            var duration =item_width;
            console.info('duration',duration)
            var progress_limit=duration-duration/2;
            setTimeout(function(){
                console.info('upload arr now is',file_upload_arr[i])
                if(result >=duration-10){delay=0}
                else if(result >  progress_limit){ delay=200000; ;}
                var now = new Date().getTime()-start;
                 var progress=now/delay;   
                 result+=progress*duration

                 item.style.width=+result+'px';   

                if( file_upload_arr[i] !=undefined &&  file_upload_arr[i] =='error'){
                       item.style.width=duration+'px';
                        item.style.backgroundColor='red';

                }else if( file_upload_arr[i] !=undefined &&  file_upload_arr[i]!='error'){      
                    console.info('image ',i,'loaded')
                    item.style.width=duration+'px';
                      item.innerHTML='<p style="opacity:0.6; text-align:center;">загрузка завершена</p>';


                }else{
                        setTimeout(arguments.callee,10)

                }


             },10) 
    }


               console.info('progres created')
               
               anim(i)
}



         /**********actions***********/     
FileLoader.Buttons.prototype.actions=function(arg){
    
                 var change_fnc=function(e){  
   
                    if(e.target.classList.contains('fileloader-load-btn')){
                    var event = document.createEvent('MouseEvents');

                    // Define that the event name is 'click'.
                    event.initEvent('click', true, true);
                     load1.dispatchEvent(event)

                    }
                    
                    if(e.target.classList.contains('jsfl_save_file')){document.querySelector('.fileloader_info').style.display='none'}
               }
                for(var i in arg){
                    if(i == 'load'){
                        var load=document.querySelector('.fileloader-load-btn');
                        var load1=document.querySelector(arg[i])
                        if(load1 ==undefined){console.error('неверно укзаны селеткор для действия загрузки');return false;}
                        var files_arr=[];
                   }
                    
                    if(i=='save'){
                 
                        var save_btn=document.querySelector('.jsfl_save_file')
                        
                    }
                }
                
           //add only one eventlistener
  if(!this.st){
         this.st=1;
         load1.addEventListener('change',function(){  
             
             setTimeout(function(){    var area=document.querySelector('.jsfl-files-wrap');
         var h=area.scrollHeight;

                 area.scrollTop=h},600)
         })
         if(load != undefined){load.addEventListener('click',function(e){change_fnc(e);
           

                
             
             })}
         if(save_btn != undefined){save_btn.addEventListener('click',function(e){
                 
                 change_fnc(e)})}


}
            }
            
            

       



 
 
//                 FileLoader.prototype.success=function(arg,i)   {
//            arg['description'] != undefined ?  this.main_wrap.setAttribute('style',this.main_wrap.getAttribute('style')+"height:110px") : this.main_wrap.setAttribute('style',this.main_wrap.getAttribute('style')+"height:100px");
// 
//                
//         } 
//         
//         FileLoader.prototype.remove=function(){
//      
//             var root=this.area_root;
//          root.innerHTML('');
//             
//         }
//         
       
 