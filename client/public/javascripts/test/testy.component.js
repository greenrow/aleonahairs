"use strict";
var TestyComponent = (function () {
    function TestyComponent(heroService) {
        this.heroService = heroService;
        this.mode = 'Observable';
    }
    TestyComponent.prototype.ngOnInit = function () { this.getHeroes(); };
    TestyComponent.prototype.getHeroes = function () {
        var _this = this;
        this.heroService.getHeroes()
            .subscribe(function (heroes) { return _this.heroes = heroes; }, function (error) { return _this.errorMessage = error; });
    };
    TestyComponent.prototype.addHero = function (name) {
        var _this = this;
        if (!name) {
            return;
        }
        this.heroService.addHero(name)
            .subscribe(function (hero) { return _this.heroes.push(hero); }, function (error) { return _this.errorMessage = error; });
    };
    return TestyComponent;
}());
exports.TestyComponent = TestyComponent;
