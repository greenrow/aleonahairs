import { Component ,Input, Output,AfterViewInit} from '@angular/core';
import {Http} from '@angular/http';
import { Injectable } from '@angular/core';
import * as slider from "./anim_slider";
import {URLSearchParams,RequestOptions,Headers} from '@angular/http';
import * as img_desc from "./app.bigimg.component";
  
 
export class Hero {
  id: number;
  name: string;
}   
export class Imgpath {
 _id: number;
  path: string;
}      
      
const ARR=[];

@Component({ 
  selector: "appy",


templateUrl:"/static/views/layout.html"

})    
@Injectable()
export class AppComponent   implements AfterViewInit{

img_desc='poor';  
xxxx={id:1}
anim=slider.animate
 iss=true
  title = 'tesr of Heroes'; 
  test='Template <script>alert("evil never sleeps")</script>Syntax';
  bigimg:any;
   canshow=false;
_this=this;
  list :Imgpath [];
  projectarr:any[];
  newsarr:any[];
   descfiles :any;
   newshow=false;
newscontent:any;
   fileslength:number;

  i=2;  

constructor( private _http: Http)  {

    _http.get('/files/').subscribe( (res) => {
        console.log(res) 
          let item = JSON.parse(res["_body"])
               console.info('ok loaded!'); 
              
    this.projectarr=item[0];
   this.newsarr=item[1];
  
  
   
 

//this.iss=true;
 setTimeout(()=>{this.iss=false},2000)
 
      }, //For Success Response
                    err => {console.error(err)}
                    
                      )//For Error Response);
  }



//events// 
      
clickev(ev){ 
  var target_=ev.target;
    while(!target_.classList.contains('project_images')){
      
       target_=target_.parentNode
     
    }
     
if(target_.classList.contains('project_images')){
            this.canshow=!this.canshow;
        let regexp=/\/static\/public\/projects\/(.+)\/(.+)/g;
        let id=target_.id;
       
        let qp = new URLSearchParams();
        qp.append('proj_id', id);
        let options = new RequestOptions({ search: qp });
        this._http.get('/files/proj/',options).subscribe((res)=>{
            let item = JSON.parse(res['_body']);
            let files_arr=item[0].files;
             this.fileslength=files_arr.length;
            this.descfiles=files_arr;

        }) 
      
} 
 
}     
getnews(e){
   

    var news_id=e.target.nodeName=="ARTICLE"?e.target.id : e.target.parentNode.id; 
     let qp = new URLSearchParams();
     qp.append('news_id', news_id);
     let options = new RequestOptions({ search: qp });
        this._http.get('/files/getnews/',options).subscribe((res)=>{
            this.newshow=true;
            
            let item = JSON.parse(res['_body']);
         this.newscontent=item[0].content;
 window.scrollTo(0,0)
        }) 
    
}
 closenews(e){
     
     var item=e.target.getAttribute('data-click-item');
     this[item]=false;
    
 }
 
 
     ngAfterViewInit (){
                 var evt = new Event("angularloaded", {"bubbles":true, "cancelable":false});
                 setTimeout(function(){
                     document.dispatchEvent(evt);
                 },50)
     };

}


