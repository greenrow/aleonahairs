import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import {AppBigimgComponent} from './app.bigimg.component'

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
HttpModule
  ], 
  declarations: [
    AppComponent,
    AppBigimgComponent
    
  
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
