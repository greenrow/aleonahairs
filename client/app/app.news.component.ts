    import { Component, Input, Output, EventEmitter} from '@angular/core';
  import { AppComponent } from './app.component';
    @Component({
      selector: 'project_desc',
      template: `
   <div class='project_foto_wrap'[style.display]="canshow_ch ? 'block' : 'none'"  >
 <div *ngIf='canshow_ch' class='glyphicon glyphicon-chevron-right arrow arrow_right'></div><div *ngIf='canshow_ch' class='glyphicon glyphicon-chevron-left arrow arrow_left hide' ></div>
    <p (click)="close_proj($event)" class='close_project'>x</p>

      <div  *ngIf='canshow_ch' [attr.l]='0' class='project_foto_no_fixed_width' [style.left.px]='0'  [style.width.%]='100*fileslength' >    
   <div  class='project_item' [style.width.%]="(100/fileslength)-(1*2)" *ngFor="let image of descfiles">
  
           <div class='image_back'[ngStyle]= "{'background': 'url('+image[0]+') no-repeat center/contain'}"></div>
           <div class='image_desctiption'><p>{{image[1]}}</p></div>
                   </div>
                   </div>
                     </div>
                     
      `     
          
    }) 
    export class AppBigimgComponent {
         @Input() yy=6;
    @Input() canshow_ch: boolean;
  @Input() descfiles: any;
   @Input() descfiles1: any;
  @Input() fileslength: any;
  @Output() canshow_chChange= new EventEmitter<any>();
  
  close_proj(e){
      
this.canshow_ch=false;
this.canshow_chChange.emit(false)
  }
    }  