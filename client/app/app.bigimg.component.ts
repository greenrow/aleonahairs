    import { Component, Input, Output, EventEmitter} from '@angular/core';
  import { AppComponent } from './app.component';
    @Component({
      selector: 'project_desc',
      template: `
   <div class='project_foto_wrap'[style.display]="canshow_ch ? 'block' : 'none'"  >
 <div *ngIf='canshow_ch' (click)='moveslider($event)' class='glyphicon glyphicon-chevron-right arrow arrow_right'></div><div *ngIf='canshow_ch' (click)='moveslider($event)' class='glyphicon glyphicon-chevron-left arrow arrow_left hide' ></div>
    <p (click)="close_proj($event)" class='close_parent'>x</p>

      <div  *ngIf='canshow_ch' [attr.l]='0' class='project_foto_no_fixed_width' [style.left.px]='0'  [style.width.%]='100*fileslength' >    
   <div  class='project_item' [style.width.%]="(100/fileslength)" *ngFor="let image of descfiles">

           <div class='image_back'[ngStyle]= "{'background': 'url('+image.path+') no-repeat center/contain'}">1</div>
           <div class='image_desctiption'><p>{{image.desc}}</p></div>
                   </div>
                   </div>
                     </div>
                     
      `      
          
    }) 
    export class AppBigimgComponent {
         @Input() yy=6;
    @Input() canshow_ch: boolean;
  @Input() descfiles: any; 
   @Input() descfiles1: any;
  @Input() fileslength: any;
  @Output() canshow_chChange= new EventEmitter<any>();
  @Output() descfilesChange= new EventEmitter<any>();
    close_proj(e){ 
   this.descfiles='';   
this.canshow_ch=false; 
this.canshow_chChange.emit(false)
this.descfilesChange.emit('')
  }
  moveslider(e){  
     let target=e.target.classList;
     let elem_width=100*this.fileslength; 
     let current_pos=parseInt($('.project_foto_no_fixed_width').attr('l'));
     if(target.contains('arrow_right')){
     
         if(current_pos== -(elem_width-200)){$('.arrow_right').addClass('hide')}
            current_pos=current_pos-100;
            $('.arrow_left').removeClass('hide')
           $('.project_foto_no_fixed_width').css('left',current_pos+'%');  $('.project_foto_no_fixed_width').attr('l',current_pos)     }
     if(target.contains('arrow_left')){  
          if(current_pos==-100){$('.arrow_left').addClass('hide')}
          if($('.arrow_right').hasClass('hide')){$('.arrow_right').removeClass('hide')}
          current_pos+=100;
      $('.project_foto_no_fixed_width').css('left',current_pos+'%')
       $('.project_foto_no_fixed_width').attr('l',current_pos)
     } 
   


  }
 
    }     
    