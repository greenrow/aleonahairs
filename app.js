var express = require('express');
var path = require('path');
const MongoClient = require('mongodb').MongoClient;
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
 
var db_func=require('./db/db_func/mongo_func.js');
 var news = require('./routes/admin_news');
var routes = require('./routes/index');
var users = require('./routes/users');  
var admin = require('./routes/admin');  
var drop = require('./routes/drop');  
var create_collection=require('./db/collections/coll.js');
var file_route = require('./routes/files');  
var assert = require('assert');
var getRawBody = require('raw-body');

         
//manipulations with db
  
   
// Connection URL
const url = 'mongodb://localhost:27017';
     
// Database Name
const dbName = 'alenahairsdb';
 
// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
  assert.equal(null, err);
  console.log("Connected successfully to server");
 
  const db = client.db(dbName);
//create_collection(db)
}); 

   



var app =  express();  


   
//coll_data 

  
    
// ***************view engine setup*********************//
var mustacheExpress = require('mustache-express');

// Register '.mustache' extension with The Mustache Express
app.engine('html', mustacheExpress());
var pathToRegexp = require('path-to-regexp')
app.set('view engine', 'html');
app.set('views', __dirname + '/client/views/');

app.use('/static', express.static(__dirname +'/client/'));


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

 
app.use('/', routes);
app.use('/users', users);
app.use('/admin', admin);
app.use('/news', news);
app.use('/files/', file_route);
app.use('/drop', drop);

 

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}
 
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {} 
  });
});


module.exports = app;
