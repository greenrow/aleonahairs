var express = require('express');
var router = express.Router();
router.get('/doc/all', function(req, res) {
  var query = "SELECT * FROM lectures";
  db.query(query,function(err,rows){
    if (!err) {
      console.log("User request all Docs");
      res.json(rows);
      }
    else
      console.log('Error while performing Query.');
  });
});

router.get('/doc/:docId/answers', function(req, res) {
  var query = "SELECT  answers.vopr_id AS qid, answers.id, " +
      "answers.text AS atext, answers.sequence, answers.truth  " +
      "FROM answers, questions " +
      "WHERE answers.vopr_id = questions.id " +
      "AND questions.doc_id = ?";

  var docID = [req.params.docId];
  console.log("User request answers for document with ID: " + req.params.docId);
  query = mysql.format(query,docID);
  db.query(query,function(err,rows){
    if (!err)
      res.json(rows);
    else
      console.log('Error while performing Query.');
  });
});
module.exports = router;