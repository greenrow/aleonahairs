var gulp = require('gulp'),
nodemon = require('gulp-nodemon'),
 //sass = require('gulp-sass'),
 ts = require("gulp-typescript"),
 tsProject = ts.createProject("./client/tsconfig.json"),
 source = require('vinyl-source-stream'),
 browserify = require('browserify'),
 //browserifyshim=require('browserify-shim')
 tsify = require("tsify"),
minify = require('gulp-minify');
  
//saas//netsta
//gulp.task('sass',function () {
//  return gulp.src(['./client/public/sass/main.scss'])
//
//    .pipe(sass().on('error', sass.logError))
//    .pipe(gulp.dest('./client/public/css/'));
//    console.log('sass')
//});  
//
//gulp.task('sass:watch',['sass'], function () {
//
//    gulp.watch('./public/sass/' ); 
//});   
       
 
  gulp.task("ts", function () {
       console.log('ts')  
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("./client/dist/"));
});    
  
gulp.task("browserfy",['ts'], function () {
    console.log('browerfy')
 return browserify({ entries: ['./client/dist/main.js'] })
     
        .bundle() 
        .pipe(source('main.bundled.js'))
        .pipe(gulp.dest('./client/dist/'));;
}) 
 
//start server & autoreload//
gulp.task('start',['watch','browserfy'], function () {
   nodemon({ "script":"./bin/www.js",
         
 
     "ext": "ts js json html",
    "restartable": "rs",
    })
    .on('restart', function () {
      console.log('restarted!')
    })  
      

})
gulp.task('watch',function(){
      gulp.watch('./client/public/ts/js.ts',['ts'])
      gulp.watch('./client/app/**/*',['browserfy'])
//   gulp.watch('./client/public/sass/main.scss',['sass'])
}) 
gulp.task('build', ['start']);
